
// lista os links e armazena
[].forEach.call(
    document.querySelectorAll('.listItems li a'),
    function (el) {
        el.addEventListener('click', function(e){
            e.preventDefault();
            var indicador = this.getAttribute('data-item');
            // limpa o active da lista de links            
            [].forEach.call(
                document.querySelectorAll('.listItems li a'),
                function (el) {
                    el.setAttribute('data-state', '');
                }
            );
            
            // limpa active de todos carousel 
            [].forEach.call(
                document.querySelectorAll('.carousel'),
                function (el) {
                    el.setAttribute('data-state', '');
                }
            );
            
            // limpa o active da lista de slides            
            [].forEach.call(
                document.querySelectorAll('.slide'),
                function (el) {
                    el.setAttribute('data-state', '');
                    el.style.opacity=0;
                }
            );
            
            // limpa o active da lista de botões            
            [].forEach.call(
                document.querySelectorAll('.indicator'),
                function (el) {
                    el.setAttribute('data-state', '');
                }
            );
            
            // ativa o link clicado e seus correspondentes             
            this.setAttribute('data-state', 'active');
            // ativa o caroulse correspondente 
            [].forEach.call(
                document.querySelectorAll("[data-carousel='"+indicador+"']"),
                function (el) {
                    el.setAttribute('data-state', 'active');
                    el.querySelector("[data-s-item= '1']").style.opacity=1;
                    el.querySelector("[data-s-item='1']").setAttribute('data-state', 'active');
                    el.querySelector("[data-slide='1']").setAttribute('data-state', 'active');
                }
            );
            // var currentCarousel = document.querySelector("[data-carousel='"+indicador+"']");             
        });
    }
);

// cria função insert after
function insertAfter(el, referenceNode) {
    referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);
}

[].forEach.call(
    document.querySelectorAll('.listItems li a'),
    function (el) {
        var indicador = el.getAttribute('data-item');
        // move o carrousel para dentro da lista no responsivo
        console.log(indicador);
        var currentCarousel = document.querySelector("[data-carousel='"+indicador+"']");             
        var carouselClone = currentCarousel.cloneNode(true);
        var alvo  = el;
        var carrouselChild = el.querySelector("[data-carousel='"+indicador+"']");
        
        if (carrouselChild == null) {
            console.log("O elemento nao existe então criamos em #pai");
            insertAfter(carouselClone, alvo.lastChild); 
        }
        
        
        function moveSlide() {
            var w = window.innerWidth;
            if (w < 1023 ) {
                // currentCarousel.style.display = 'none';
                // carouselClone.style.display = 'flex';
                
            }
            if (w > 1023) {
                // currentCarousel.style.display = 'flex';
                // carouselClone.style.display = 'none';
            }
        }
        
        moveSlide();
        window.addEventListener('resize', function(){
            moveSlide();
        });
    }
);

var texto = document.querySelector('.textSlide');
var textoClone = texto.cloneNode(true);
var alvo  = document.querySelector('.listItems ');
alvo.insertBefore(textoClone, alvo.firstChild); 
function detectatela() {
    var w = window.innerWidth;
    if (w < 1023 ) {
        texto.style.display = 'none';
        textoClone.style.display = 'flex';
        
    }
    if (w > 1023) {
        texto.style.display = 'flex';
        textoClone.style.display = 'none';
    }
    console.log(parseInt(w));
}
detectatela();
window.addEventListener('resize', function(){
    detectatela();
});

var speed    = 7000; // velocidade do slide


// verifica todos carrousel e inicia eles 
[].forEach.call(
    document.querySelectorAll('.carousel'),
    function (el) {
        console.log(carousel);
        var carousel = el;
        var lis      = carousel.querySelectorAll('indicators input');
        var count    = lis.length;
        var slides   = count;
        
        function carouselHide(num) {
            indicators[num].setAttribute('data-state', '');
            slides[num].setAttribute('data-state', '');
            // links[num].setAttribute('data-state', '');
            slides[num].style.opacity=0;
        }
        
        
        function carouselShow(num) {
            indicators[num].checked = true;
            indicators[num].setAttribute('data-state', 'active');
            slides[num].setAttribute('data-state', 'active');
            // links[num].setAttribute('data-state', 'active');
            slides[num].style.opacity=1;
        }
        
        
        
        function setSlide(slide) {
            return function() {
                // Resetando todos slides
                for (var i = 0; i < indicators.length; i++) {
                    indicators[i].setAttribute('data-state', '');
                    slides[i].setAttribute('data-state', '');
                    
                    carouselHide(i);
                }
                
                // definindo slide ativo
                indicators[slide].setAttribute('data-state', 'active');
                slides[slide].setAttribute('data-state', 'active');
                carouselShow(slide);
                
                // para o auto-switcher
                clearInterval(switcher);
            };
        }
        
        function switchSlide() {
            var nextSlide = 0;
            
            // reseta todos slides
            for (var i = 0; i < indicators.length; i++) {
                // Se o slide atual estiver ativo e NÃO for igual ao último slide, incrementa nextSlide
                if ((indicators[i].getAttribute('data-state') == 'active') && (i !== (indicators.length-1))) {
                    nextSlide = i + 1;
                }
                
                // removo todos ativos 
                carouselHide(i);
            }
            
            // defini o proximo como ativo e mostra ele
            carouselShow(nextSlide);
        }
        
        if (carousel) {
            var slides = carousel.querySelectorAll('.slide');
            var indicators = carousel.querySelectorAll('.indicator');
            
            var switcher = setInterval(function() {
                switchSlide();
            }, speed);
            
            for (var i = 0; i < indicators.length; i++) {
                indicators[i].addEventListener("click", setSlide(i));
            }
        }
        
    }
);

// validando formulario
function enviadados(){
    
    if(document.dados.nome.value=="" || document.dados.nome.value.length < 8)
    {
        alert( "Preencha campo NOME corretamente!" );
        document.dados.nome.focus();
        return false;
    }
    
    
    if( document.dados.email.value=="" || document.dados.email.value.indexOf('@')==-1 || document.dados.email.value.indexOf('.')==-1 || document.dados.email.value.length < 8)
    {
        alert( "Preencha campo E-MAIL corretamente!" );
        document.dados.email.focus();
        return false;
    }
   
    return true;
}

// anima o scroll
var btnMenu  = document.querySelector('.menu-pri');
var abreMenu = document.querySelector('header ul');
var linkMenu = document.querySelector('header ul');

linkMenu.addEventListener('click', function(){
    abreMenu.classList.toggle('fademenu');
    btnMenu.classList.toggle('active');
});

// limpa o active da lista de botões            



btnMenu.addEventListener('click', function(e){
    this.classList.toggle('active');
    abreMenu.classList.toggle('fademenu');
    e.preventDefault();
});
